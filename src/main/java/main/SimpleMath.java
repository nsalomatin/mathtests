package main;

import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Created by Cok on 04.06.2016.
 */
public class SimpleMath {

    @Step("Run addition")
    public static long addition(long operand1, long operand2) {
        return operand1+operand2;
    }

    @Step("Run deduction")
    public static long deduction(long operand1, long operand2) {
        return operand1-operand2;
    }

    @Step("Run multiplication")
    public static long multiplication(long operand1, long operand2) {
        return operand1*operand2;
    }

    @Step("Run devision")
    public static long devision(long operand1, long operand2) {
        if (operand2==0) {
            return 0;
        } else {
            return operand1/operand2;
        }

    }
}
