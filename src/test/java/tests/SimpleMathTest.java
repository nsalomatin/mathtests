package tests;


import dataProviders.MathDataProvider;
import main.SimpleMath;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Parameter;
import ru.yandex.qatools.allure.annotations.Stories;

import static org.testng.Assert.assertEquals;

/**
 * Created by Cok on 06.06.2016.
 */
@Features("Mathematic operations")
public class SimpleMathTest {

    @Stories("Addition")
    @Test(dataProvider = "mathOperationsForSimpleTests", dataProviderClass = MathDataProvider.class)
    public static void additionTest(@Parameter("Operand1")long operand1,
                                    @Parameter("Operand2")long operand2,
                                    @Parameter("Operation")String operation,
                                    @Parameter("Result")long result) {

        assertEquals(SimpleMath.addition(operand1, operand2), result);
    }

    @Stories("Deduction")
    @Test(dataProvider = "mathOperationsForSimpleTests", dataProviderClass = MathDataProvider.class)
    public void deductionTest(@Parameter("Operand1")long operand1,
                              @Parameter("Operand2")long operand2,
                              @Parameter("Operation")String operation,
                              @Parameter("Result")long result) {
        assertEquals(SimpleMath.deduction(operand1, operand2), result);
    }

    @Stories("Multiplication")
    @Test(dataProvider = "mathOperationsForSimpleTests", dataProviderClass = MathDataProvider.class)
    public void multiplicationTest(@Parameter("Operand1")long operand1,
                              @Parameter("Operand2")long operand2,
                              @Parameter("Operation")String operation,
                              @Parameter("Result")long result) {
        assertEquals(SimpleMath.multiplication(operand1, operand2), result);
    }

    @Stories("Devision")
    @Test(dataProvider = "mathOperationsForSimpleTests", dataProviderClass = MathDataProvider.class)
    public void devisionTest(@Parameter("Operand1")long operand1,
                                   @Parameter("Operand2")long operand2,
                                   @Parameter("Operation")String operation,
                                   @Parameter("Result")long result) {
        assertEquals(SimpleMath.devision(operand1, operand2), result);
    }
}
