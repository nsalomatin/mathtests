package tests.withFactory;


import main.SimpleMath;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Parameter;
import ru.yandex.qatools.allure.annotations.Stories;

import static org.testng.Assert.*;

/**
 * Created by Cok on 06.06.2016.
 */
@Features("Mathematic operations")
public class AdditionTest extends AbstractFactoryMathTest {


    public AdditionTest(long operand1, long operand2, long result) {
        super(operand1, operand2, result);
    }

    @Stories("Addition")
    @Test
    public void additionTest() {
        assertEquals(SimpleMath.addition(operand1, operand2), result);
    }
}
