package tests.withFactory;

import dataProviders.MathDataProvider;
import org.testng.annotations.Factory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cok on 06.06.2016.
 */

public class FactoryForTests {

    @Factory(dataProvider = "mathOperationsForFactory", dataProviderClass = MathDataProvider.class)
    public static Object[] createInstances(long operand1, long operand2, String operation, long operationResult) {

        List<Object> tests = new ArrayList<>();
        switch (operation) {
            case "+":
                tests.add(new AdditionTest(operand1, operand2, operationResult));
                break;
            case "-":
                tests.add(new DeductionTest(operand1,operand2, operationResult));
                break;
            case "*":
                tests.add(new MultiplicationTest(operand1,operand2, operationResult));
                break;
            case "/":
                tests.add(new DevisionTest(operand1,operand2, operationResult));
                break;
        }

        return tests.toArray();
    }


}
