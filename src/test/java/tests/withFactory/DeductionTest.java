package tests.withFactory;


import main.SimpleMath;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static org.testng.Assert.assertEquals;

/**
 * Created by Cok on 06.06.2016.
 */
@Features("Mathematic operations")
public class DeductionTest extends AbstractFactoryMathTest {

    public DeductionTest(long operand1, long operand2, long result) {
        super(operand1, operand2, result);
    }


    @Stories("Deduction")
    @Test
    public void deductionTest() {
        assertEquals(SimpleMath.deduction(operand1, operand2), result);
    }
}
