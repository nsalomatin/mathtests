package tests.withFactory;

/**
 * Created by Cok on 06.06.2016.
 */

public abstract class AbstractFactoryMathTest extends AbstractMathTest {

    protected long operand1;
    protected long operand2;
    protected long result;

    public AbstractFactoryMathTest(long operand1, long operand2, long result) {

        this.operand1 = operand1;
        this.operand2 = operand2;
        this.result = result;
    }
}
