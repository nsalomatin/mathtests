package dataProviders;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Cok on 06.06.2016.
 */
public class MathDataProvider extends AbstractDataProvider {

    @DataProvider()
    public static Object[][] mathOperationsForSimpleTests(Method method) {

        Iterator<Object[]> iterator = CSVDataProvider.getDataAsList("src\\test\\resources\\MathOperations.csv").iterator();
        String methodName = method.getName();

        switch (methodName) {

            case "additionTest":
                return getData(iterator, "+");

            case "deductionTest":
                return getData(iterator, "-");

            case "devisionTest":
                return getData(iterator, "/");

            case "multiplicationTest":
                return getData(iterator, "*");

            default:
                return new Object[1][1];
        }
    }

    @DataProvider()
    public static Object[][] mathOperationsForFactory() {
        List<Object[]> testData = CSVDataProvider.getDataAsList("src\\test\\resources\\MathOperations.csv");
        return mapToObject(testData, 4);
    }

    private static Object[][] getData(Iterator<Object[]> iterator, String expectedOperation) {
        List<Object[]> testDataList = new ArrayList<>();
        Object[] columnsFromCSV = new Object[1][0];

        while (iterator.hasNext()) {
            columnsFromCSV = iterator.next();
            String operation = columnsFromCSV[2].toString();
            if (expectedOperation.equals(operation)) {
                testDataList.add(columnsFromCSV);
            }
        }
        return mapToObject(testDataList, columnsFromCSV.length);
    }

    private static Object[][] mapToObject(List<Object[]> objects, int countOfColumn) {
        int i = 0;
        Object[][] dataObjects = new Object[objects.size()][countOfColumn];

        for (Object[] columns : objects) {
            for (int count=0; count<countOfColumn; count++) {
                dataObjects[i][count] = columns[count];
            }
            i++;
        }
        return dataObjects;
    }
}
