package dataProviders;

import com.csvreader.CsvReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Cok on 07.06.2016.
 */
public class CSVDataProvider extends AbstractDataProvider {

    private static CsvReader csvReader = null;

    public static List<Object[]> getDataAsList(String fileName) {
        int i = 0;
        List<Object[]> dataFromCsv = new ArrayList<>();
        try {
            csvReader = new CsvReader(fileName);
            csvReader.setDelimiter(';');
            while (csvReader.readRecord()) {
                dataFromCsv.add(new Object[]{
                        Long.valueOf(csvReader.get(0)),
                        Long.valueOf(csvReader.get(1)),
                        csvReader.get(2),
                        Long.valueOf(csvReader.get(3))});
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return dataFromCsv;
    }
}
