# README #

MathTests for testing simple math operations

### How do I get set up? ###

Launch the main implementation:
run: mvn clean test site

Launch the factory implementation:
run: mvn clean test site -P factory